#!/usr/bin/env python

import logging

from pinnacle import APIClient
from telegram import Update
from telegram.ext import Application, MessageHandler, ContextTypes, filters

from secrets import TELEGRAM_SECRET, USER, PASSWORD, STAKE

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)


def pick_side(side: str) -> str:
    if side == 'H':
        return 'Team1'
    elif side == 'A':
        return 'Team2'
    else:
        return 'Draw'


async def place_bet(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    api = APIClient(USER, PASSWORD)
    rows_per_bet = 4
    bets_data = update.channel_post.text.split('\n')
    bets_count = int(len(bets_data) / rows_per_bet)
    for i in range(bets_count):
        match_info = bets_data[i*rows_per_bet].split(' | ')
        data = bets_data[3 + i*rows_per_bet]
        bet_status = bets_data[2 + i*rows_per_bet].split('.')[0]
        should_bet = bet_status == 'Pastatyta'
        pick = data.split(' ')[2]
        line_id, event_id = data.split(' ')[4].split(',')
        team = pick_side(pick)
        if should_bet:
            api.betting.place_bet(
                sport_id=29, event_id=event_id, line_id=line_id,
                period_number=0, bet_type="MONEYLINE", stake=STAKE,
                team=team, fill_type="Normal"
            )
        logger.info(f"{bet_status} | {match_info[2]} | {match_info[1]} | {pick} | {STAKE}")


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(TELEGRAM_SECRET).build()

    bets_handler = MessageHandler(filters.TEXT, place_bet)
    application.add_handler(bets_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()